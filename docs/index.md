# Python 소개: 연혁과 응용 <sup>[1](#footnote_1)</sup>

> <font size="3">Python의 기원, 기능 및 사용 사례에 대해 알아본다.</font>

## 목차
1. [Python이란 무엇이며 왜 인기가 있을까?](./introduction.md#python이란-무엇이며-왜-인기가-있을까)
1. [어떻게 Python은 탄생했을까?](./introduction.md#어떻게-python은-탄생했을까)
1. [Python의 주요 특징은?](./introduction.md#python의-주요-특징은)
1. [Python이 널리 사용되는 분야는?](./introduction.md#python이-널리-사용되는-분야는)
1. [어떻게 Python은 시작할 수 있나?](./introduction.md#어떻게-python은-시작할-수-있나)

    References

<a name="footnote_1">1</a>: [Python for Beginners](https://medium.com/@ocrnshn/python-for-beginners-1299a610029f)를 편역한 것이다.
